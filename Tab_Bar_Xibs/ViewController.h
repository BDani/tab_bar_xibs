//
//  ViewController.h
//  Tab_Bar_Xibs
//
//  Created by Bruno Tavares on 23/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) NSString *labelString;
@property (strong, nonatomic) IBOutlet UILabel *label;

@end

