//
//  ViewController.m
//  Tab_Bar_Xibs
//
//  Created by Bruno Tavares on 23/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.label.text = self.labelString;
}

@end
